﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Net;
using System.Net.Http;
using Autofac;
using System.Reflection;
using Yudoo.Infrastructure.Services;
using Yudoo.Infrastructure.Repositories;
using Autofac.Integration.WebApi;

namespace WebApplication1
{
    public static class AutofacWebapiConfig
    {
        public static IContainer Container;

        public static void Initialize(HttpConfiguration config)
        {
            Initialize(config, RegisterServices(new ContainerBuilder()));
        }


        public static void Initialize(HttpConfiguration config, IContainer container)
        {
           config.DependencyResolver = new AutofacWebApiDependencyResolver(container);
        }

        private static IContainer RegisterServices(ContainerBuilder builder)
        {
            //Register your Web API controllers.  
           builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            //builder.RegisterGeneric(typeof(BankServices))
            //       .As(typeof(IBankServices))
            //       .InstancePerRequest();

            builder.RegisterType<BankServices>().As<IBankServices>().InstancePerDependency();
            builder.RegisterType<ListServices>().As<IListServices>().InstancePerDependency();
            builder.RegisterType<PermissionServices>().As<IPermissionServices>().InstancePerDependency();
            builder.RegisterType<ProductService>().As<IProductServices>().InstancePerDependency();
            builder.RegisterType<ProductTypeServices>().As<IProductTypeServices>().InstancePerDependency();
            builder.RegisterType<ReviewServices>().As<IReviewServices>().InstancePerDependency();
            builder.RegisterType<SellerServices>().As<ISellerServices>().InstancePerDependency();

            builder.RegisterType<BankRepositories>().As<IBankRepositories>().InstancePerDependency();
            builder.RegisterType<ListRepositiories>().As<IListRepositories>().InstancePerDependency();
            builder.RegisterType<PermissionRepositiories>().As<IPermissionRepositiories>().InstancePerDependency();
            builder.RegisterType<ProductRepositiories>().As<IProductRepositiories>().InstancePerDependency();
            builder.RegisterType<ProductTypeRepositiories>().As<IProductTypeRepositiories>().InstancePerDependency();
            builder.RegisterType<ReviewRepositiories>().As<IReviewRepositiories>().InstancePerDependency();
            builder.RegisterType<SellerRepositories>().As<ISellerRepositories>().InstancePerDependency();

            //Set the dependency resolver to be Autofac.  
            Container = builder.Build();

            return Container;
        }
    }
}
