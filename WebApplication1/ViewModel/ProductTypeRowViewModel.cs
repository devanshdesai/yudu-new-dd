﻿using Newtonsoft.Json;

namespace Yudoo.ViewModel
{
    public class ProductTypeRowViewModel
    {
        [JsonProperty("Product_Type_Id")]
        public int ProductTypeId { get; set; }
        [JsonProperty("Product_Type_Name")]
        public string ProductTypeName { get; set; }
        [JsonProperty("Created_By")]
        public string CreatedBy { get; set; }
        [JsonProperty("Created_Date")]
        public string CreatedDate { get; set; }
        [JsonProperty("Product_Sub_Type_Id")]
        public string ProductSubTypeId { get; set; }
        [JsonProperty("Parent_Id")]
        public string ParentId { get; set; }
    }
}
