﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Yudoo.ViewModel
{
    public class ProductTypeListViewModel
    {
        [JsonPropertyName("total_count")]
        public int Total { get; internal set; }

        [JsonPropertyName("rows")]
        public List<ProductTypeRowViewModel> Rows { get; set; }
    }
}
