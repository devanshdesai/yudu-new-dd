﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Yudoo.ViewModel
{
    public class ProductListViewModel
    {
        [JsonPropertyName("total_count")]
        public int Total { get; internal set; }

        [JsonPropertyName("rows")]
        public List<ProductRowViewModel> Rows { get; set; }
    }
}
