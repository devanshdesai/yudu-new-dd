﻿using System;
using Newtonsoft.Json;

namespace Yudoo.ViewModel
{
    public class UpsertReviewViewModel
    {
        [JsonProperty("Review_Id")]
        public int ReviewId { get; set; }
        [JsonProperty("Review_Comment")]
        public string ReviewComment { get; set; }
        [JsonProperty("Customer_Id")]
        public int CustomerId { get; set; }
        [JsonProperty("Review_By")]
        public string ReviewBy { get; set; }
        [JsonProperty("Review_Date")]
        public DateTime ReviewDate { get; set; }
    }
}
