﻿using Newtonsoft.Json;

namespace Yudoo.ViewModel
{
    public class UpsertPermissionViewModel
    {
        [JsonProperty("Permission_Id")]
        public int PermissionId { get; set; }
        [JsonProperty("Permission_Name")]
        public string PermissionName { get; set; }
        [JsonProperty("Permission_Description")]
        public string PermissionDescription { get; set; }
        [JsonProperty("Is_Access")]
        public string IsAccess { get; set; }
        [JsonProperty("Customer_Id")]
        public int CustomerId { get; set; }
    }
}
