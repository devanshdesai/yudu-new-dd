﻿using Newtonsoft.Json;

namespace Yudoo.ViewModel
{
    public class UpsertProductTypeDetailsParamModel
    {
        [JsonProperty("Id")]
        public int Id { get; set; }
        [JsonProperty("Product_Type_Name")]
        public string ProductTypeName { get; set; }
        [JsonProperty("Created_By")]
        public int CreatedBy { get; set; }
        [JsonProperty("Product_Sub_Type_Id")]
        public int ProductSubTypeId { get; set; }
        [JsonProperty("Parent_Id")]
        public int ParentId { get; set; }
    }
}
