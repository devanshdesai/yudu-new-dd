﻿namespace Yudoo.ViewModel
{
    public class AllDetailParamModel
    {
        public string Search { get; set; }
        public long Offset { get; set; }
        public long Limit { get; set; }
    }
}
