﻿namespace Yudoo.ViewModel
{
    public class AddressUpsertParamModel
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public long PinCode { get; set; }
        public long LookupCityId { get; set; }
        public long LookupStateId { get; set; }
        public long LookupCountryId { get; set; }
        public long AddressType { get; set; }
        public long CreatedBy { get; set; }
        public long DeletedBy { get; set; }
    }
}