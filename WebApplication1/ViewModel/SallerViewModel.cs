﻿using Newtonsoft.Json;
using System;

namespace Yudoo.ViewModel
{
    public class SallerViewModel
    {
        //[JsonProperty("shop_id")]
        public int ShopId { get; set; }

        //[JsonProperty("shop_name")]
        public string ShopName { get; set; }

        //[JsonProperty("shop_owner_name")]
        public string ShopOwnerName { get; set; }


        //[JsonProperty("CategoryId")]
        public int? CategoryId { get; set; }

        //[JsonProperty("address_line_1")]
        public string AddressLine1 { get; set; }

        //[JsonProperty("address_line_2")]
        public string AddressLine2 { get; set; }

        //[JsonProperty("land_mark")]
        public string LandMark { get; set; }

        //[JsonProperty("pin_code")]
        public int? PinCode { get; set; }

        //[JsonProperty("video_call_datetime")]
        public DateTime? VideoCall { get; set; }

        //[JsonProperty("registered_date")]
        public DateTime? RegisteredDate { get; set; }

        //[JsonProperty("shop_status")]
        public bool ShopStatus { get; set; }

        //[JsonProperty("ratings")]
        public int? Ratings { get; set; }

        //[JsonProperty("ratings_by")]
        public int? RatingsBy { get; set; }

        //[JsonProperty("ratings_created_date")]
        public DateTime? RatingsCreatedDate { get; set; }

        //[JsonProperty("ratings_updated_date")]
        public DateTime? RatingsUpdatedDate { get; set; }

        //[JsonProperty("deleted_date")]
        public DateTime? DeletedDate { get; set; }

        //[JsonProperty("deleted_by")]
        public int? DeletedBy { get; set; }

        //[JsonProperty("is_deleted")]
        public bool IsDeleted { get; set; }

        //[JsonProperty("is_active")]
        public bool IsActive { get; set; }

        //[JsonProperty("gst_number")]
        public string GstNumber { get; set; }

        //[JsonProperty("mobile_number")]
        public string MobileNumber { get; set; }

        //[JsonProperty("personal_pan_number")]
        public string PersonalPanNumber { get; set; }

        //[JsonProperty("company_pan_number")]
        public string CompanyPanNumber { get; set; }
    }
}
