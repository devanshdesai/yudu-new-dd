﻿using Newtonsoft.Json;
using System;

namespace Yudoo.ViewModel
{
    public class BankViewModel
    {
        [JsonProperty("Bank_Id")]
        public int BankId { get; set; }

        [JsonProperty("shop_Id")]
        public int? shopId { get; set; }

        [JsonProperty("IsActive")]
        public bool IsActive { get; set; }

        [JsonProperty("Bank_Name")]
        public string BankName { get; set; }

        [JsonProperty("Bank_Account_No")]
        public int BankAccountNo { get; set; }

        [JsonProperty("IFSC_Code")]
        public string IFSCCode { get; set; }

        [JsonProperty("Account_Holder_Name")]
        public string AccountHolderName { get; set; }

        [JsonProperty("Created_Date")]
        public DateTime? CreatedDate { get; set; }

        [JsonProperty("Created_By")]
        public int? CreatedBy { get; set; }

        [JsonProperty("Updated_Date")]
        public DateTime? UpdatedDate { get; set; }

        [JsonProperty("Updated_By")]
        public int UpdatedBy { get; set; }
    }
}
