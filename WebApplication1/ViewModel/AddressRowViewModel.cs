﻿using System;

namespace Yudoo.ViewModel
{
    public class AddressRowViewModel
    {
        public long Id { get; set; }
        public long UserId { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public long PinCode { get; set; }
        public long LookupCityId { get; set; }
        public long LookupStateId { get; set; }
        public long LookupCountryId { get; set; }
        public long AddressType { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public long CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; }
        public long UpdatedBy { get; set; }
        public DateTime DeletedDate { get; set; }
        public long DeletedBy { get; set; }
    }
}