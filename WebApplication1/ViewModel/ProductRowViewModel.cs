﻿using System;
using Newtonsoft.Json;

namespace Yudoo.ViewModel
{
    public class ProductRowViewModel
    {
        [JsonProperty("Product_Id")]
        public int ProductId { get; set; }
        [JsonProperty("Product_Name")]
        public string ProductName { get; set; }
        [JsonProperty("Product_Type_Id")]
        public int ProductTypeId { get; set; }
        [JsonProperty("Ratings")]
        public int Ratings { get; set; }
        [JsonProperty("Ratings_By")]
        public int RatingsBy { get; set; }
        [JsonProperty("Ratings_Created_Date")]
        public DateTime RatingsCreatedDate { get; set; }
        [JsonProperty("Ratings_Updated_Date")]
        public DateTime RatingsUpdatedDate { get; set; }
        [JsonProperty("Product_Experience")]
        public int ProductExperience { get; set; }
        [JsonProperty("Product_Price")]
        public int ProductPrice { get; set; }
        [JsonProperty("Product_Description")]
        public string ProductDescription { get; set; }
        [JsonProperty("Service_Experience")]
        public int ServiceExperience { get; set; }
        [JsonProperty("Product_Weight")]
        public int ProductWeight { get; set; }
        [JsonProperty("Created_By")]
        public int CreatedBy { get; set; }
        [JsonProperty("Created_Date")]
        public DateTime CreatedDate { get; set; }
        [JsonProperty("Is_Active")]
        public bool IsActive { get; set; }
        [JsonProperty("Deleted_Date")]
        public DateTime DeletedDate { get; set; }
        [JsonProperty("Deleted_By")]
        public int DeletedBy { get; set; }
        [JsonProperty("Is_Deleted")]
        public bool IsDeleted { get; set; }
    }
}
