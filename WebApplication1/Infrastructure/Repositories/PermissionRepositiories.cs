﻿using System.Linq;
using System.Threading.Tasks;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Repositories
{
    public interface IPermissionRepositiories
    {
        Task Permission_Upsert(
                int PermissionId,
                string PermissionName,
                string PermissionDescription,
                int CustomerId
            );
        Task<UpsertPermissionViewModel> Permission_ByCustomerId(int Customer_Id);
    }
    public class PermissionRepositiories : BaseRepository, IPermissionRepositiories
    {
        public PermissionRepositiories() { }

        public async Task Permission_Upsert(
                int PermissionId,
                string PermissionName,
                string PermissionDescription,
                int CustomerId
            )
        {
            var sql = "Permission_insertUpdate";

            await ExecuteAsync(sql, new
            {
                PermissionId = PermissionId,
                PermissionName = PermissionName,
                PermissionDescription = PermissionDescription,
                CustomerId = CustomerId
            });
        }

        public async Task<UpsertPermissionViewModel> Permission_ByCustomerId(int Customer_Id)
        {
            var sql = "Permission_ByCustomerId";

            var result = await QueryAsync<UpsertPermissionViewModel>(sql, new
            {
                CustomerId = Customer_Id
            });

            return result.FirstOrDefault();
        }
    }
}
