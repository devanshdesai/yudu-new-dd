﻿using System.Linq;
using System.Threading.Tasks;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Repositories
{
    public interface IBankRepositories
    {

        Task<BankViewModel> GetBankDetail(int? shop_id);
        Task PostBankDetail_Upsert(
                int BankId,
                int ShopId,
                string BankName,
                int BankAccountNo,
                string IFSCCode,
                string AccountHolderName,
                int CreatedBy,
                int UpdatedBy
            );
        Task<bool> PostBankDetails_ActInActive(int bank_id);
    }
    public class BankRepositories : BaseRepository, IBankRepositories
    {
        public BankRepositories() { }
     
        public async Task<BankViewModel> GetBankDetail(int? shop_id)
        {
            var sql = "BankDetailsGetByShopId";

            var result = await QueryAsync<BankViewModel>(sql, new
            {
                ShopId = shop_id
            });

            return result.FirstOrDefault();
        }

        public async Task PostBankDetail_Upsert(
                int BankId,
                int ShopId,
                string BankName,
                int BankAccountNo,
                string IFSCCode,
                string AccountHolderName,
                int CreatedBy,
                int UpdatedBy
            )
        {
            var sql = "BankDetails_Upsert";

            await ExecuteAsync(sql, new
            {
                BankId = BankId,
                ShopId = ShopId,
                BankName = BankName,
                BankAccountNo = BankAccountNo,
                IFSCCode = IFSCCode,
                AccountHolderName = AccountHolderName,
                CreatedBy = CreatedBy,
                UpdatedBy = UpdatedBy
            });
        }

        public async Task<bool> PostBankDetails_ActInActive(int bank_id)
        {
            var sql = "BankDetails_ActInActive";

            var result = await QueryAsync<bool>(sql, new
            {
                BankId = bank_id
            });

            return result.FirstOrDefault();
        }
    }
}
