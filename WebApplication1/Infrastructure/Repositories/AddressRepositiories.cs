﻿using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Repositories
{
    public interface IAddressRepositiories
    {
        Task<List<AddressRowViewModel>> Address_All(string Search, long Offset, long Limit);
        Task<AddressRowViewModel> Address_ById(long Id);
        Task<AddressRowViewModel> Address_ByUserId(long UserId);
        Task Address_Upsert(
                long Id,
                long UserId,
                string AddressLine1,
                string AddressLine2,
                string AddressLine3,
                long PinCode,
                long LookupCityId,
                long LookupStateId,
                long LookupCountryId,
                long AddressType,
                long CreatedBy,
                long DeletedBy
            );
        Task<bool> Address_ActInact(long Id);
        Task<bool> Address_Delete(long Id);
    }
    public class AddressRepositiories : BaseRepository, IAddressRepositiories
    {
        public AddressRepositiories() { }

        public async Task<List<AddressRowViewModel>> Address_All(string Search, long Offset, long Limit)
        {
            var sql = "Address_All";

            var result = await QueryAsync<AddressRowViewModel>(sql, new
            {
                Search = Search,
                Offset = Offset,
                Limit = Limit
            });

            return result.ToList();
        }

        public async Task<AddressRowViewModel> Address_ById(long Id)
        {
            var sql = "Addresss_ById";

            var result = await QueryAsync<AddressRowViewModel>(sql, new
            {
                Id = Id
            });

            return result.FirstOrDefault();
        }

        public async Task<AddressRowViewModel> Address_ByUserId(long UserId)
        {
            var sql = "Addresss_ByUserId";

            var result = await QueryAsync<AddressRowViewModel>(sql, new
            {
                UserId = UserId
            });

            return result.FirstOrDefault();
        }

        public async Task Address_Upsert(
                long Id,
                long UserId,
                string AddressLine1,
                string AddressLine2,
                string AddressLine3,
                long PinCode,
                long LookupCityId,
                long LookupStateId,
                long LookupCountryId,
                long AddressType,
                long CreatedBy,
                long DeletedBy
            )
        {
            var sql = "Address_Upsert";

            await ExecuteAsync(sql, new
            {
                Id = Id,
                UserId = UserId,
                AddressLine1 = AddressLine1,
                AddressLine2 = AddressLine2,
                AddressLine3 = AddressLine3,
                PinCode = PinCode,
                LookupCityId = LookupCityId,
                LookupStateId = LookupStateId,
                LookupCountryId = LookupCountryId,
                AddressType = AddressType,
                CreatedBy = CreatedBy,
                DeletedBy = DeletedBy
            });
        }
        public async Task<bool> Address_ActInact(long Id)
        {
            var sql = "Address_ActInact";

            var result = await QueryAsync<bool>(sql, new
            {
                Id = Id
            });

            return result.FirstOrDefault<bool>();
        }
        public async Task<bool> Address_Delete(long Id)
        {
            var sql = "Addresss_Delete";

            var result = await QueryAsync<bool>(sql, new
            {
                Id = Id
            });

            return result.FirstOrDefault<bool>();
        }
    }
}
