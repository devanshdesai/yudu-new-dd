﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Yudoo.Infrastructure.Repositories.ListResult;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Repositories
{
    public interface IProductTypeRepositiories
    {
        Task<ListResultGeneric<ProductTypeRowViewModel>> GetAll_details(long Offset, long Limit);
        Task<ProductTypeRowViewModel> GetDetail(int Product_Type_Id);
        Task PostProductTypeDetail_Upsert(
                int Id,
                string ProductTypeName,
                int CreatedBy,
                int ProductSubTypeId,
                int ParentId
            );
    }
    public class ProductTypeRepositiories : BaseRepository, IProductTypeRepositiories
    {
        public ProductTypeRepositiories() { }

        public async Task<ListResultGeneric<ProductTypeRowViewModel>> GetAll_details(long Offset, long Limit)
        {
            var sql = "ProductType_All";

            var parameters = new DynamicParameters();
            parameters.Add("@Offset", Offset);
            parameters.Add("@Limit", Limit);
            parameters.Add("@Total", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            var result = await QueryAsync<ProductTypeRowViewModel>(sql, parameters);

            return new ListResultGeneric<ProductTypeRowViewModel>
            {
                Rows = result.ToList(),
                Total = parameters.Get<int>("@Total")
            };
        }

        public async Task<ProductTypeRowViewModel> GetDetail(int Product_Type_Id)
        {
            var sql = "ProductType_ById";

            var result = await QueryAsync<ProductTypeRowViewModel>(sql, new
            {
                ProductTypeId = Product_Type_Id
            });

            return result.FirstOrDefault();
        }

        public async Task PostProductTypeDetail_Upsert(
                int Id,
                string ProductTypeName,
                int CreatedBy,
                int ProductSubTypeId,
                int ParentId
            )
        {
            var sql = "ProductType_insertUpdate";

            await ExecuteAsync(sql, new
            {
                Id = Id,
                ProductTypeName = ProductTypeName,
                CreatedBy = CreatedBy,
                ProductSubTypeId = ProductSubTypeId,
                ParentId = ParentId
            });
        }
    }
}
