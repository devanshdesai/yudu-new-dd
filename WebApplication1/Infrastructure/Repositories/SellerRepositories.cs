﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Repositories
{
    public interface ISellerRepositories
    {
        Task<bool> GetRegisteredShopDetails(string name, string shop_name, string mobile);


        Task<bool> checkExistsShop(int? shop_id, string mobile);
        Task<SallerViewModel> AddUpdateShopOwner(int? id, string shop_name, string shop_owner_name, int? main_category_id,
            string address_line_1, string address_line_2, string land_mark, int? pin_code, int? ratings, int? ratings_by,
            int? deleted_by, string gst_number, string mobile_number, string personal_pan_number, string company_pan_number);

        Task<bool> ShopOwnerActinAct(int? shop_id);

        Task<SallerViewModel> GetSellerDetails(int? shop_id);
        Task<bool> checkSellerExists(int? shop_id);
    }
    public class SellerRepositories : BaseRepository,ISellerRepositories
    {
        public SellerRepositories()
        { 
            
        }


        public async Task<bool> GetRegisteredShopDetails(string name, string shop_name, string mobile)
        {
            var sql = "check_seller_exists";

            var result = await QueryAsync<int>(sql, new
            {
                shopownerName = name,
                shopName = shop_name,
                mobileNumber = mobile
            });

            return result.Any();
        }

        public async Task<bool> ShopOwnerActinAct(int? shop_id)
        {
            var sql = "ShopOwner_ActInAct";

            var result = await QueryAsync<bool>(sql, new
            {
               Id = shop_id
            });

            return result.Any();
        }

        public async Task<bool> checkExistsShop(int? shop_id, string mobile)
        {
            var sql = "check_seller_exists_by_shopid_mobile";

            var result = await QueryAsync<int>(sql, new
            {
                shopid = shop_id,
                mobile = mobile
            });

            return result.Any();
        }

        public async Task<SallerViewModel> GetSellerDetails(int? shop_id)
        {
            var sql = "Seller_by_shop_Id";

            var result = await QueryAsync<SallerViewModel>(sql, new
            {
                Id = shop_id
            });

            return result.FirstOrDefault();
        }

        public async Task<bool> checkSellerExists(int? shop_id)
        {
            var sql = "check_seller_exists_by_shop_id";

            var result = await QueryAsync<SallerViewModel>(sql, new
            {
                shop_id = shop_id
            });

            return result.Any();
        }




        public async Task<SallerViewModel> AddUpdateShopOwner(int? id, string shop_name, string shop_owner_name, int? main_category_id,
            string address_line_1, string address_line_2, string land_mark, int? pin_code, int? ratings, int? ratings_by,
            int? deleted_by, string gst_number, string mobile_number, string personal_pan_number, string company_pan_number)
        {
            var sql = "ShopOwner_insertUpdate";

            var result = await QueryAsync<SallerViewModel>(sql, new
            {
                ShopId = id,
                ShopName = shop_name,
                ShopOwnerName = shop_owner_name,
                MainCategoryId = main_category_id,
                AddressLine1 = address_line_1,
                AddressLine2 = address_line_2,
                LandMark = land_mark,
                PinCode = pin_code,
                Ratings = ratings,
                RatingsBy = ratings_by,
                MobileNumber = mobile_number,
                DeletedBy = deleted_by,
                GstNumber = gst_number,
                PersonalPanNumber = personal_pan_number,
                CompanyPanNumber = company_pan_number
            });

            return result.FirstOrDefault();
        }
    }
}
