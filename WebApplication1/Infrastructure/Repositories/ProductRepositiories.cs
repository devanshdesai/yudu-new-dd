﻿using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Yudoo.Infrastructure.Repositories.ListResult;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Repositories
{
    public interface IProductRepositiories
    {
        Task<ListResultGeneric<ProductRowViewModel>> GetAllProduct_details(long Offset, long Limit);
        Task<ProductRowViewModel> GetProduct_details(int Product_Id);
        Task PostProductDetail_Upsert(
                int Id,
                string ProductName,
                int ProductTypeId,
                int Ratings,
                int RatingsBy,
                int ProductExperience,
                int ProductPrice,
                string ProductDescription,
                int ServiceExperience,
                int ProductWeight,
                int CreatedBy,
                int DeletedBy
            );
        Task<bool> PostProduct_ActInActive(int id);
        Task<bool> PostProduct_Delete(int id);
    }
    public class ProductRepositiories : BaseRepository, IProductRepositiories
    {
        public ProductRepositiories() { }

        public async Task<ListResultGeneric<ProductRowViewModel>> GetAllProduct_details(long Offset, long Limit)
        {
            var sql = "Products_All";

            var parameters = new DynamicParameters();
            parameters.Add("@Offset", Offset);
            parameters.Add("@Limit", Limit);
            parameters.Add("@Total", dbType: DbType.Int32, direction: ParameterDirection.ReturnValue);

            var result = await QueryAsync<ProductRowViewModel>(sql, parameters);

            return new ListResultGeneric<ProductRowViewModel>
            {
                Rows = result.ToList(),
                Total = parameters.Get<int>("@Total")
            };
        }

        public async Task<ProductRowViewModel> GetProduct_details(int Product_Id)
        {
            var sql = "Products_ById";

            var result = await QueryAsync<ProductRowViewModel>(sql, new
            {
                ProductId = Product_Id
            });

            return result.FirstOrDefault();
        }

        public async Task PostProductDetail_Upsert(
                int Id,
                string ProductName,
                int ProductTypeId,
                int Ratings,
                int RatingsBy,
                int ProductExperience,
                int ProductPrice,
                string ProductDescription,
                int ServiceExperience,
                int ProductWeight,
                int CreatedBy,
                int DeletedBy
            )
        {
            var sql = "Products_insertUpdate";

            await ExecuteAsync(sql, new
            {
                Id= Id,
                ProductName= ProductName,
                ProductTypeId= ProductTypeId,
                Ratings= Ratings,
                RatingsBy= RatingsBy,
                ProductExperience= ProductExperience,
                ProductPrice= ProductPrice,
                ProductDescription= ProductDescription,
                ServiceExperience= ServiceExperience,
                ProductWeight= ProductWeight,
                CreatedBy= CreatedBy,
                DeletedBy= DeletedBy
            });
        }
        public async Task<bool> PostProduct_ActInActive(int id)
        {
            var sql = "Products_ActInAct";

            var result = await QueryAsync<bool>(sql, new
            {
                Id = id
            });

            return result.FirstOrDefault();
        }
        public async Task<bool> PostProduct_Delete(int id)
        {
            var sql = "Products_Delete";

            var result = await QueryAsync<bool>(sql, new
            {
                Id = id
            });

            return result.FirstOrDefault();
        }
    }
}
