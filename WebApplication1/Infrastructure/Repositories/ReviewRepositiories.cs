﻿using System.Linq;
using System.Threading.Tasks;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Repositories
{
    public interface IReviewRepositiories
    {
        Task Review_Insert(
                int ReviewId,
                string ReviewComment,
                int CustomerId
            );
        Task<UpsertReviewViewModel> Review_ByCustomerId(int Customer_Id);
    }
    public class ReviewRepositiories : BaseRepository, IReviewRepositiories
    {
        public ReviewRepositiories() { }

        public async Task Review_Insert(
                int ReviewId,
                string ReviewComment,
                int CustomerId
            )
        {
            var sql = "Review_insert";

            await ExecuteAsync(sql, new
            {
                ReviewId = ReviewId,
                ReviewComment = ReviewComment,
                CustomerId = CustomerId
            });
        }

        public async Task<UpsertReviewViewModel> Review_ByCustomerId(int Customer_Id)
        {
            var sql = "Review_ByCustomerId";

            var result = await QueryAsync<UpsertReviewViewModel>(sql, new
            {
                CustomerId = Customer_Id
            });

            return result.FirstOrDefault();
        }
    }
}
