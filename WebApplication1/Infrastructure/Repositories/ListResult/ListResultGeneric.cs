﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Yudoo.Infrastructure.Repositories.ListResult
{
    public class ListResultGeneric<T> where T : class
    {
        public int Total { get; set; }
        public List<T> Rows { get; set; }
    }
}
