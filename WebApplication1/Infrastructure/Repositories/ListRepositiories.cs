﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Repositories
{
    public interface IListRepositories
    {
        Task<ProductTypeListViewModel> GetProductTypeByParentId(int parent_id);
    }
    public class ListRepositiories : BaseRepository, IListRepositories
    {
        public ListRepositiories()
        {

        }
        public async Task<ProductTypeListViewModel> GetProductTypeByParentId(int parent_id)
        {
            var sql = "GetCategorybyParentId";

            var result = await QueryAsync<ProductTypeListViewModel>(sql, new
            {
                ParentId = parent_id
            });

            return result.FirstOrDefault();
        }
    }
}
