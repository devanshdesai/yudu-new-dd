﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Repositories;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Services
{
    public interface IBankServices
    {
        Task<BankViewModel> GetBankDetail(int? shop_id);
        Task PostBankDetail_Upsert(UpsertBankDetailsParamModel paramModel);
        Task<bool> PostBankDetails_ActInActive(int bank_id);
    }
    public class BankServices : IBankServices
    {
        private readonly IBankRepositories _BankRepository;

        public BankServices(IBankRepositories BankRepository)
        {
            _BankRepository = BankRepository;
        }

        public async Task<BankViewModel> GetBankDetail(int? shop_id)
        {
            var result = await _BankRepository.GetBankDetail(shop_id);
            return result;
        }

        public async Task PostBankDetail_Upsert(UpsertBankDetailsParamModel param)
        {
            await _BankRepository.PostBankDetail_Upsert(
                    BankId: param.BankId,
                    ShopId: param.ShopId,
                    BankName: param.BankName,
                    BankAccountNo: param.BankAccountNo,
                    IFSCCode: param.IFSCCode,
                    AccountHolderName: param.AccountHolderName,
                    CreatedBy: param.CreatedBy,
                    UpdatedBy: param.UpdatedBy
            );
        }
        public async Task<bool> PostBankDetails_ActInActive(int bank_id)
        {
            var result = await _BankRepository.PostBankDetails_ActInActive(bank_id);
            if(result)
                return true;
            else
                throw new NotFoundException("bank details not found.");
        }
    }
}
