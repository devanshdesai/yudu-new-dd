﻿using System.Threading.Tasks;
using Yudoo.Infrastructure.Repositories;
using Yudoo.ViewModel;
using System.Linq;
using System.Collections.Generic;

namespace Yudoo.Infrastructure.Services
{
    public interface IAddressServices
    {
        Task<List<AddressRowViewModel>> Address_All(AllDetailParamModel param);
        Task<AddressRowViewModel> Address_ById(long Id);
        Task<AddressRowViewModel> Address_ByUserId(long UserId);
        Task Address_Upsert(AddressUpsertParamModel paramModel);
        Task<bool> Address_ActInact(long Id);
        Task<bool> Address_Delete(long Id);
    }
    public class AddressService : IAddressServices
    {
        private readonly IAddressRepositiories _AddressRepository;

        public AddressService(IAddressRepositiories AddressRepository)
        {
            _AddressRepository = AddressRepository;
        }

        public async Task<List<AddressRowViewModel>> Address_All(AllDetailParamModel param)
        {
            var resultList = await _AddressRepository.Address_All(
                    Search: param.Search,
                    Offset: param.Offset,
                    Limit: param.Limit
            );

            return resultList.Select(c => new AddressRowViewModel
            {
                Id = c.Id,
                UserId = c.UserId,
                AddressLine1 = c.AddressLine1,
                AddressLine2 = c.AddressLine2,
                AddressLine3 = c.AddressLine3,
                PinCode = c.PinCode,
                LookupCityId = c.LookupCityId,
                LookupStateId = c.LookupStateId,
                LookupCountryId = c.LookupCountryId,
                AddressType = c.AddressType,
                IsActive = c.IsActive,
                CreatedDate = c.CreatedDate,
                CreatedBy = c.CreatedBy,
                UpdatedDate = c.UpdatedDate,
                UpdatedBy = c.UpdatedBy,
                DeletedDate = c.DeletedDate,
                DeletedBy = c.DeletedBy
            }).ToList();

        }

        public async Task<AddressRowViewModel> Address_ById(long Id)
        {
            var result = await _AddressRepository.Address_ById(Id);
            return result;
        }

        public async Task<AddressRowViewModel> Address_ByUserId(long UserId)
        {
            var result = await _AddressRepository.Address_ByUserId(UserId);
            return result;
        }

        public async Task Address_Upsert(AddressUpsertParamModel param)
        {
            await _AddressRepository.Address_Upsert(
                    Id: param.Id,
                    UserId: param.UserId,
                    AddressLine1: param.AddressLine1,
                    AddressLine2: param.AddressLine2,
                    AddressLine3: param.AddressLine3,
                    PinCode: param.PinCode,
                    LookupCityId: param.LookupCityId,
                    LookupStateId: param.LookupStateId,
                    LookupCountryId: param.LookupCountryId,
                    AddressType: param.AddressType,
                    CreatedBy: param.CreatedBy,
                    DeletedBy: param.DeletedBy
            );
        }

        public async Task<bool> Address_ActInact(long Id)
        {
            return await _AddressRepository.Address_ActInact(Id);
        }

        public async Task<bool> Address_Delete(long Id)
        {
            return await _AddressRepository.Address_Delete(Id);
        }
    }
}
