﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Repositories;
using Yudoo.ViewModel;
using System.Linq;

namespace Yudoo.Infrastructure.Services
{
    public interface IProductServices
    {
        Task<ProductListViewModel> GetAllProduct_details(AllDetailParamModel param);
        Task<ProductRowViewModel> GetProduct_details(int Product_Id);
        Task PostProductDetail_Upsert(UpsertProductDetailsParamModel paramModel);
        Task<bool> PostProduct_ActInActive(int id);
        Task<bool> PostProduct_Delete(int id);
    }
    public class ProductService : IProductServices
    {
        private readonly IProductRepositiories _ProductRepository;

        public ProductService(IProductRepositiories ProductRepository)
        {
            _ProductRepository = ProductRepository;
        }

        public async Task<ProductListViewModel> GetAllProduct_details(AllDetailParamModel param)
        {
            var resultList = await _ProductRepository.GetAllProduct_details(
                    Offset: param.Offset,
                    Limit: param.Limit
            );

            var rows = resultList.Rows.Select(c => new ProductRowViewModel
            {
                ProductId = c.ProductId,
                ProductName = c.ProductName,
                ProductTypeId = c.ProductTypeId,
                Ratings = c.Ratings,
                RatingsBy = c.RatingsBy,
                RatingsCreatedDate = c.RatingsCreatedDate,
                RatingsUpdatedDate = c.RatingsUpdatedDate,
                ProductExperience = c.ProductExperience,
                ProductPrice = c.ProductPrice,
                ProductDescription = c.ProductDescription,
                ServiceExperience = c.ServiceExperience,
                ProductWeight = c.ProductWeight,
                CreatedBy = c.CreatedBy,
                CreatedDate = c.CreatedDate,
                IsActive = c.IsActive,
                DeletedDate = c.DeletedDate,
                DeletedBy = c.DeletedBy,
                IsDeleted = c.IsDeleted,
            }).ToList();

            return new ProductListViewModel
            {
                Total = resultList.Total,
                Rows = rows
            };
        }

        public async Task<ProductRowViewModel> GetProduct_details(int Product_Id)
        {
            var result = await _ProductRepository.GetProduct_details(Product_Id);
            return result;
        }

        public async Task PostProductDetail_Upsert(UpsertProductDetailsParamModel param)
        {
            await _ProductRepository.PostProductDetail_Upsert(
                    Id: param.Id,
                    ProductName: param.ProductName,
                    ProductTypeId: param.ProductTypeId,
                    Ratings: param.Ratings,
                    RatingsBy: param.RatingsBy,
                    ProductExperience: param.ProductExperience,
                    ProductPrice: param.ProductPrice,
                    ProductDescription: param.ProductDescription,
                    ServiceExperience: param.ServiceExperience,
                    ProductWeight: param.ProductWeight,
                    CreatedBy: param.CreatedBy,
                    DeletedBy: param.DeletedBy
            );
        }

        public async Task<bool> PostProduct_ActInActive(int id)
        {
            return await _ProductRepository.PostProduct_ActInActive(id);
        }

        public async Task<bool> PostProduct_Delete(int id)
        {
            return await _ProductRepository.PostProduct_Delete(id);
        }
    }
}
