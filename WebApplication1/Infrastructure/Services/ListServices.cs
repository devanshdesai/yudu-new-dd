﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Repositories;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Services
{
    public interface IListServices
    {
        Task<ProductTypeListViewModel> GetProductTypeByParentId(int parent_id);
    }
    public class ListServices : IListServices
    {
        private readonly IListRepositories _ListRepository;

        public ListServices(IListRepositories ListRepository)
        {
            _ListRepository = ListRepository;
        }
        public async Task<ProductTypeListViewModel> GetProductTypeByParentId(int parent_id)
        {
            var result = await _ListRepository.GetProductTypeByParentId(parent_id);
            return result;
        }
    }
}
