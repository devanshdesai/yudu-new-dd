﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Repositories;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Services
{

    public interface ISellerServices
    {
        Task<SallerViewModel> GetRegisteredShopDetails(string name, string shop_name, string mobile);

        Task<bool> checkExistsShop(int? shop_id, string mobile);
        Task<SallerViewModel> GetSellerDetails(int? shop_id);
        Task<bool> UpdateGSTShopDetails(int? shop_id, string gst_number, string company_register_address, string company_pan_number, string personal_pan_number,bool is_register);
        Task<bool> ShopownerActinAct(int? shop_id);
    }
    public class SellerServices : ISellerServices
    {
        private readonly ISellerRepositories _sellerRepository;

        public SellerServices(ISellerRepositories sellerRepository)
        {
            _sellerRepository = sellerRepository;
        }

        public async Task<SallerViewModel> GetRegisteredShopDetails(string name, string shop_name, string mobile)
        {
            var exists = await _sellerRepository.GetRegisteredShopDetails(name, shop_name, mobile);

            if (exists)
            {
                SallerViewModel sallerViewModel = new SallerViewModel();
                return sallerViewModel;
            }
            else
            {
                var result = await _sellerRepository.AddUpdateShopOwner(0, shop_name, name, null, null, null, null, null, null, null, null,null,mobile,null,null);
                return result;
            }
        }


        public async Task<bool> checkExistsShop(int? shop_id, string mobile)
        {
            var exists = await _sellerRepository.checkExistsShop(shop_id, mobile);
            if (exists)
                return true;
            else
                throw new NotFoundException("shop not found.");
        }

        public async Task<SallerViewModel> GetSellerDetails(int? shop_id)
        {
            var result = await _sellerRepository.GetSellerDetails(shop_id);
            return result;
        }

        public async Task<bool> UpdateGSTShopDetails(int? shop_id, string gst_number, string company_register_address, string company_pan_number, string personal_pan_number, bool is_register)
        {
            var exists = await _sellerRepository.checkSellerExists(shop_id);
            if (is_register)
            {
                if (exists)
                {
                    var result = await _sellerRepository.GetSellerDetails(shop_id);
                    await _sellerRepository.AddUpdateShopOwner(result.ShopId, result.ShopName, result.ShopOwnerName, 
                        result.CategoryId, result.AddressLine1,result.AddressLine2, result.LandMark, result.PinCode, 
                        result.Ratings, result.RatingsBy,result.DeletedBy, gst_number, result.MobileNumber, 
                        personal_pan_number, company_pan_number);
                    return true;
                }
                else
                {
                    throw new NotFoundException("shop not found.");
                }
            }
            else
            {
                var result = await _sellerRepository.GetSellerDetails(shop_id);
                await _sellerRepository.AddUpdateShopOwner(result.ShopId, result.ShopName, result.ShopOwnerName,
                    result.CategoryId, result.AddressLine1,result.AddressLine2, result.LandMark, result.PinCode,
                    result.Ratings,result.RatingsBy,result.DeletedBy, result.GstNumber, result.MobileNumber, 
                    personal_pan_number,result.PersonalPanNumber);
                return true;
            }
        }

        public async Task<bool> ShopownerActinAct(int? shop_id)
        {
            var exists = await _sellerRepository.checkSellerExists(shop_id);
          
            if (exists)
                return true;
            else
                throw new NotFoundException("shop not found.");
        }
    }
}
