﻿using System.Threading.Tasks;
using Yudoo.Infrastructure.Repositories;
using Yudoo.ViewModel;
using System.Linq;

namespace Yudoo.Infrastructure.Services
{
    public interface IProductTypeServices
    {
        Task<ProductTypeListViewModel> GetAll_details(AllDetailParamModel param);
        Task<ProductTypeRowViewModel> GetDetail(int Product_Type_Id);
        Task PostProductTypeDetail_Upsert(UpsertProductTypeDetailsParamModel paramModel);
    }
    public class ProductTypeServices : IProductTypeServices
    {
        private readonly IProductTypeRepositiories _ProductTypeRepository;

        public ProductTypeServices(IProductTypeRepositiories ProductTypeRepository)
        {
            _ProductTypeRepository = ProductTypeRepository;
        }

        public async Task<ProductTypeListViewModel> GetAll_details(AllDetailParamModel param)
        {
            var resultList = await _ProductTypeRepository.GetAll_details(
                    Offset: param.Offset,
                    Limit: param.Limit
            );

            var rows = resultList.Rows.Select(c => new ProductTypeRowViewModel
            {
                ProductTypeId = c.ProductTypeId,
                ProductTypeName = c.ProductTypeName,
                CreatedBy = c.CreatedBy,
                CreatedDate = c.CreatedDate,
                ProductSubTypeId = c.ProductSubTypeId,
                ParentId = c.ParentId
            }).ToList();

            return new ProductTypeListViewModel
            {
                Total = resultList.Total,
                Rows = rows
            };
        }

        public async Task<ProductTypeRowViewModel> GetDetail(int Product_Type_Id)
        {
            var result = await _ProductTypeRepository.GetDetail(Product_Type_Id);
            return result;
        }

        public async Task PostProductTypeDetail_Upsert(UpsertProductTypeDetailsParamModel param)
        {
            await _ProductTypeRepository.PostProductTypeDetail_Upsert(
                    Id: param.Id,
                    ProductTypeName: param.ProductTypeName,
                    CreatedBy: param.CreatedBy,
                    ProductSubTypeId: param.ProductSubTypeId,
                    ParentId: param.ParentId
            );
        }
    }
}
