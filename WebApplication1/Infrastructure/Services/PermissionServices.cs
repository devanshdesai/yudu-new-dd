﻿using System.Threading.Tasks;
using Yudoo.Infrastructure.Repositories;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Services
{
    public interface IPermissionServices
    {
        Task Permission_Upsert(UpsertPermissionViewModel paramModel);
        Task<UpsertPermissionViewModel> Permission_ByCustomerId(int Customer_Id);
    }
    public class PermissionServices : IPermissionServices
    {
        private readonly IPermissionRepositiories _PermissionRepository;

        public PermissionServices(IPermissionRepositiories PermissionRepository)
        {
            _PermissionRepository = PermissionRepository;
        }

        public async Task Permission_Upsert(UpsertPermissionViewModel param)
        {
            await _PermissionRepository.Permission_Upsert(
                    PermissionId: param.PermissionId,
                    PermissionName: param.PermissionName,
                    PermissionDescription: param.PermissionDescription,
                    CustomerId: param.CustomerId
            );
        }

        public async Task<UpsertPermissionViewModel> Permission_ByCustomerId(int Customer_Id)
        {
            var result = await _PermissionRepository.Permission_ByCustomerId(Customer_Id);
            return result;
        }
    }
}
