﻿using System.Threading.Tasks;
using Yudoo.Infrastructure.Repositories;
using Yudoo.ViewModel;

namespace Yudoo.Infrastructure.Services
{
    public interface IReviewServices
    {
        Task Review_Insert(UpsertReviewViewModel paramModel);
        Task<UpsertReviewViewModel> Review_ByCustomerId(int Customer_Id);
    }
    public class ReviewServices : IReviewServices
    {
        private readonly IReviewRepositiories _ReviewRepository;

        public ReviewServices(IReviewRepositiories ReviewRepository)
        {
            _ReviewRepository = ReviewRepository;
        }

        public async Task Review_Insert(UpsertReviewViewModel param)
        {
            await _ReviewRepository.Review_Insert(
                    ReviewId: param.ReviewId,
                    ReviewComment: param.ReviewComment,
                    CustomerId: param.CustomerId
            );
        }

        public async Task<UpsertReviewViewModel> Review_ByCustomerId(int Customer_Id)
        {
            var result = await _ReviewRepository.Review_ByCustomerId(Customer_Id);
            return result;
        }
    }
}
