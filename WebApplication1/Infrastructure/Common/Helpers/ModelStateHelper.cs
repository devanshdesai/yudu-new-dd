﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;

namespace Yudoo.Infrastructure.Common.Helpers
{
    public static class ModelStateHelper
    {
        public static IDictionary<string, string[]> Errors(this ModelStateDictionary modelState, object model)
        {
            if (!modelState.IsValid)
            {
                var dictionary = modelState.ToDictionary(k => k.Key, v => v.Value)
                    .Where(v => v.Value.ValidationState == ModelValidationState.Invalid)
                    .ToDictionary(
                    k =>
                    {
                        var property = model.GetType().GetProperties().FirstOrDefault(p => p.Name.Equals(k.Key, StringComparison.InvariantCultureIgnoreCase));
                        if (property != null)
                        {
                            //Try to get the attribute  
                            var displayName = property.GetCustomAttributes(typeof(JsonPropertyNameAttribute), true).Cast<JsonPropertyNameAttribute>().SingleOrDefault()?.Name;
                            return displayName ?? property.Name;
                        }

                        return k.Key; //Nothing found, return original vaidation key  
                    },
                    v => v.Value.Errors.Select(e => e.ErrorMessage).ToArray()); //Box String collection  

                return dictionary;
            }
            return null;
        }
    }
}
