﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Yudoo.Infrastructure.Common.ResponseModels
{
    public class ResponseObject<T>
    {
        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("data")]
        public T Data { get; set; }

        [JsonPropertyName("errors")]
        public IDictionary<string, string[]> Errors { get; set; }
    }
}
