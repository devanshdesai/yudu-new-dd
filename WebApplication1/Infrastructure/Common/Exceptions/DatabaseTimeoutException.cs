﻿using System;

namespace Hippo.InternalApp.Infrastructure.Common.Exceptions
{
    public class DatabaseTimeoutException : Exception
    {
        public DatabaseTimeoutException(string message)
            : base(message)
        {

        }

    }
}
