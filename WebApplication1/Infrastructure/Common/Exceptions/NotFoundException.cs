﻿using System;

namespace Hippo.InternalApp.Infrastructure.Common.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string message)
            : base(message) { }
    }
}
