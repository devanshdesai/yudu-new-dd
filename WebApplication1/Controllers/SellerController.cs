﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.Helpers;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Services;
using Yudoo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;
using System.Web;

namespace Yudoo.Controller
{
    public class SellerController : ApiController
    {
        private readonly ISellerServices _SellerService;
        public SellerController(ISellerServices SellerService)
        {
            _SellerService = SellerService;
        }


        [HttpGet]
        [Route("api/Seller/seller_otp")]
        public async Task<IHttpActionResult> GetRegisteredShopDetails(string name, string shop_name, string mobile)
        {
            try
            {
                var result = await _SellerService.GetRegisteredShopDetails(name, shop_name, mobile);
                if (result != null)
                {
                    //return Ok(new ResponseObject<int>
                    //{
                    //    Status = "ok",
                    //    Data = ConversionHelper.GenerateOtp()
                    //});

                    return Ok(new ResponseObject<SallerViewModel>
                    {
                        Status = "ok",
                        Data = result
                    });
                }
                else
                {
                    throw new NotFoundException("Seller Inserted Successfully");
                }

            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }


        [HttpGet]
        [Route("api/Seller/exists_seller")]
        public async Task<IHttpActionResult> checkExistsShop(int? shop_id, string mobile)
        {
            try
            {
                var result = await _SellerService.checkExistsShop(shop_id, mobile);
                if (result)
                {
                    var sellerData = await _SellerService.GetSellerDetails(shop_id);
                    return Ok(new ResponseObject<SallerViewModel>
                    {
                        Status = "ok",
                        Data = sellerData
                    });
                }
                else
                {
                    throw new NotFoundException("Seller Not Found");
                }

            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/Seller/update_gst_shop_detail")]
        public async Task<IHttpActionResult> UpdateGSTShopDetails([FromUri]int? shop_id, [FromUri] string gst_number, [FromUri] string company_register_address, [FromUri] string company_pan_number, [FromUri] string personal_pan_number, [FromUri] bool is_register)
        {
            try
            {
                var result = await _SellerService.UpdateGSTShopDetails(shop_id, gst_number, company_register_address, company_pan_number, personal_pan_number, is_register);

                return Ok(new ResponseObject<bool>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPut]
        [Route("api/Seller/ShopOwnerActinAct")]
        public async Task<IHttpActionResult> GetShopownerActinAct(int? shop_id)
        {
            try
            {
                var result = await _SellerService.ShopownerActinAct(shop_id);

                return Ok(new ResponseObject<bool>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Seller/SellerDetails")]
        public async Task<IHttpActionResult> GetSellerDetails(int? shop_id)
        {
            try
            {
                var result = await _SellerService.GetSellerDetails(shop_id);

                return Ok(new ResponseObject<SallerViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Seller/Users_SendOTP")]
        public async Task<IHttpActionResult> Users_SendOTP(string mobilenumber)
        {
            string otp = mobilenumber == "9924088018" ? "1234" : new Random().Next(0, 9999).ToString("D4");
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(HttpUtility.UrlDecode(ConfigurationManager.AppSettings["smsapi"]).Replace("#mobile#", mobilenumber).Replace("#otp#", otp).ToString());
            request.Method = "GET";
            request.ContentType = "application/json";

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();

                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine(e.Message);
            }

            return this.Content((HttpStatusCode)HttpStatusCode.OK, otp);
        }

        [HttpGet]
        [Route("api/Seller/GetGSTInfo")]
        public async Task<IHttpActionResult> GetGSTInfo(string gstnumber)
        {

            string apiUrl = Convert.ToString(ConfigurationManager.AppSettings["gstapi"]).Replace("#gstnum#", gstnumber);
            GSTInfo message = new GSTInfo();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiUrl);
            request.Method = "GET";
            request.ContentType = "application/json";

            try
            {
                WebResponse webResponse = request.GetResponse();
                using (Stream webStream = webResponse.GetResponseStream() ?? Stream.Null)
                using (StreamReader responseReader = new StreamReader(webStream))
                {
                    string response = responseReader.ReadToEnd();
                    message = JsonConvert.DeserializeObject<GSTInfo>(response);

                }
            }
            catch (Exception e)
            {
                Console.Out.WriteLine("-----------------");
                Console.Out.WriteLine(e.Message);
            }

            return this.Content((HttpStatusCode)HttpStatusCode.OK, message);
        }
    }



    public class Addr
    {
        public string bnm { get; set; }
        public string st { get; set; }
        public string loc { get; set; }
        public string bno { get; set; }
        public string stcd { get; set; }
        public string dst { get; set; }
        public string city { get; set; }
        public string flno { get; set; }
        public string lt { get; set; }
        public string pncd { get; set; }
        public string lg { get; set; }
    }

    public class Pradr
    {
        public Addr addr { get; set; }
        public string ntr { get; set; }
    }

    public class TaxpayerInfo
    {
        public string stjCd { get; set; }
        public string lgnm { get; set; }
        public string stj { get; set; }
        public string dty { get; set; }
        public List<object> adadr { get; set; }
        public string cxdt { get; set; }
        public List<string> nba { get; set; }
        public string gstin { get; set; }
        public string lstupdt { get; set; }
        public string rgdt { get; set; }
        public string ctb { get; set; }
        public Pradr pradr { get; set; }
        public string tradeNam { get; set; }
        public string sts { get; set; }
        public string ctjCd { get; set; }
        public string ctj { get; set; }
        public string panNo { get; set; }
    }

    public class Compliance
    {
        public object filingFrequency { get; set; }
    }

    public class GSTInfo
    {
        public TaxpayerInfo taxpayerInfo { get; set; }
        public Compliance compliance { get; set; }
        public List<object> filing { get; set; }
    }

}
