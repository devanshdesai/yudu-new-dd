﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Services;
using Yudoo.ViewModel;
using System.Collections.Generic;
using System.Web.Http;

namespace Yudoo.Controller
{
    public class AddressController : ApiController
    {
        private readonly IAddressServices _AddressService;
        public AddressController(IAddressServices AddressService)
        {
            _AddressService = AddressService;
        }

        [HttpGet]
        [Route("api/Address/Address_All")]
        public async Task<IHttpActionResult> Address_All(string Search, long Offset, long Limit)
        {
            AllDetailParamModel param = new AllDetailParamModel();
            param.Search = Search;
            param.Offset = Offset;
            param.Limit = Limit;

            try
            {
                var result = await _AddressService.Address_All(param);

                return Ok(new ResponseObject<List<AddressRowViewModel>>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Address/Address_ById")]
        public async Task<IHttpActionResult> Address_ById(long Id)
        {
            try
            {
                var result = await _AddressService.Address_ById(Id);

                return Ok(new ResponseObject<AddressRowViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Address/Address_ByUserId")]
        public async Task<IHttpActionResult> Address_ByUserId(long UserId)
        {
            try
            {
                var result = await _AddressService.Address_ByUserId(UserId);

                return Ok(new ResponseObject<AddressRowViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/Address/Address_Upsert")]
        public async Task<IHttpActionResult> Address_Upsert([FromBody] AddressUpsertParamModel paramModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Validation Error");
                }

                await _AddressService.Address_Upsert(paramModel);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/Address/Address_ActInact")]
        public async Task<IHttpActionResult> Address_ActInact(long Id)
        {
            try
            {
                var result = await _AddressService.Address_ActInact(Id);

                return Ok(new ResponseObject<object>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/Address/Address_Delete")]
        public async Task<IHttpActionResult> Address_Delete(long Id)
        {
            try
            {
                var result = await _AddressService.Address_Delete(Id);

                return Ok(new ResponseObject<object>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
