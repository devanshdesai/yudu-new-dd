﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.Helpers;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Services;
using Yudoo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Yudoo.Controller
{
    public class ProductController : ApiController
    {
        private readonly IProductServices _ProductService;
        public ProductController(IProductServices ProductService)
        {
            _ProductService = ProductService;
        }

        [HttpGet]
        [Route("api/product/All_Product_details")]
        public async Task<IHttpActionResult> GetAllProduct_details(long Offset, long Limit)
        {
            AllDetailParamModel param = new AllDetailParamModel();
            param.Offset = Offset;
            param.Limit = Limit;

            try
            {
                var result = await _ProductService.GetAllProduct_details(param);

                return Ok(new ResponseObject<ProductListViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/product/Product_details")]
        public async Task<IHttpActionResult> GetProduct_details(int Product_Id)
        {
            try
            {
                var result = await _ProductService.GetProduct_details(Product_Id);

                return Ok(new ResponseObject<ProductRowViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/product/ProductDetail_Upsert")]
        public async Task<IHttpActionResult> PostProductDetail_Upsert([FromBody] UpsertProductDetailsParamModel paramModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Validation Error");
                }

                await _ProductService.PostProductDetail_Upsert(paramModel);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/product/Product_ActInActive")]
        public async Task<IHttpActionResult> PostProduct_ActInActive([FromBody] int id)
        {
            try
            {
                var result = await _ProductService.PostProduct_ActInActive(id);

                return Ok(new ResponseObject<object>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/product/Product_Delete")]
        public async Task<IHttpActionResult> PostProduct_Delete([FromBody] int id)
        {
            try
            {
                var result = await _ProductService.PostProduct_Delete(id);

                return Ok(new ResponseObject<object>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
