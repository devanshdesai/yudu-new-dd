﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.Helpers;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Services;
using Yudoo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Yudoo.Controller
{
    public class ProductTypeController : ApiController
    {
        private readonly IProductTypeServices _ProductTypeService;
        public ProductTypeController(IProductTypeServices ProductTypeService)
        {
            _ProductTypeService = ProductTypeService;
        }

        [HttpGet]
        [Route("api/ProductType/All_details")]
        public async Task<IHttpActionResult> GetAll_details(long Offset, long Limit)
        {
            AllDetailParamModel param = new AllDetailParamModel();
            param.Offset = Offset;
            param.Limit = Limit;

            try
            {
                var result = await _ProductTypeService.GetAll_details(param);

                return Ok(new ResponseObject<ProductTypeListViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/ProductType/details")]
        public async Task<IHttpActionResult> GetDetail(int Product_Type_Id)
        {
            try
            {
                var result = await _ProductTypeService.GetDetail(Product_Type_Id);

                return Ok(new ResponseObject<ProductTypeRowViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [ActionName("api/ProductType/ProductTypeDetail_Upsert")]
        public async Task<IHttpActionResult> PostProductTypeDetail_Upsert([FromBody] UpsertProductTypeDetailsParamModel paramModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Validation Error");
                }

                await _ProductTypeService.PostProductTypeDetail_Upsert(paramModel);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
