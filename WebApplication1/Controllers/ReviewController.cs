﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.Helpers;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Services;
using Yudoo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;


namespace Yudoo.Controller
{
    public class ReviewController : ApiController
    {
        private readonly IReviewServices _ReviewService;
        public ReviewController(IReviewServices ReviewService)
        {
            _ReviewService = ReviewService;
        }

        [HttpPost]
        [Route("api/Review/Review_Insert")]
        public async Task<IHttpActionResult> Review_Insert([FromBody] UpsertReviewViewModel paramModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Validation Error");
                }

                await _ReviewService.Review_Insert(paramModel);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Review/Review_ByCustomerId")]
        public async Task<IHttpActionResult> Review_ByCustomerId(int Customer_Id)
        {
            try
            {
                var result = await _ReviewService.Review_ByCustomerId(Customer_Id);

                return Ok(new ResponseObject<UpsertReviewViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
