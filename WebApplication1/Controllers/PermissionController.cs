﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.Helpers;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Services;
using Yudoo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Yudoo.Controller
{
    public class PermissionController : ApiController
    {
        private readonly IPermissionServices _PermissionService;
        public PermissionController(IPermissionServices PermissionService)
        {
            _PermissionService = PermissionService;
        }

        [HttpPost]
        [Route("api/Permission/Permission_Upsert")]
        public async Task<IHttpActionResult> Permission_Upsert([FromBody] UpsertPermissionViewModel paramModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid error");
                }

                await _PermissionService.Permission_Upsert(paramModel);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("api/Permission/Permission_ByCustomerId")]
        public async Task<IHttpActionResult> Permission_ByCustomerId(int Customer_Id)
        {
            try
            {
                var result = await _PermissionService.Permission_ByCustomerId(Customer_Id);

                return Ok(new ResponseObject<UpsertPermissionViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
