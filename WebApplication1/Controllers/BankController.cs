﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
//using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.Helpers;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Services;
using Yudoo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Yudoo.Controller
{
    public class BankController : ApiController
    {
        private readonly IBankServices _BankService;
        public BankController(IBankServices BankService)
        {
            _BankService = BankService;
        }

        [HttpGet]
        [Route("api/bank/details")]
        public async Task<IHttpActionResult> GetBankDetail(int? shop_id)
        {
            try
            {
                var result = await _BankService.GetBankDetail(shop_id);

                return Ok(new ResponseObject<BankViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/bank/BankDetail_Upsert")]
        public async Task<IHttpActionResult> PostBankDetail_Upsert([FromBody] UpsertBankDetailsParamModel paramModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest("Invalid error");
                }

                await _BankService.PostBankDetail_Upsert(paramModel);
                return Ok();
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("api/bank/BankDetails_ActInActive")]
        public async Task<IHttpActionResult> PostBankDetails_ActInActive([FromBody] int bank_id)
        {
            try
            {
                var result = await _BankService.PostBankDetails_ActInActive(bank_id);

                return Ok(new ResponseObject<bool>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }

    }
}
