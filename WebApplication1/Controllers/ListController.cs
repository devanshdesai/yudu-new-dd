﻿using Hippo.InternalApp.Infrastructure.Common.Exceptions;
using System.Threading.Tasks;
using Yudoo.Infrastructure.Common.ResponseModels;
using Yudoo.Infrastructure.Services;
using Yudoo.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Yudoo.Controller
{
    public class ListController : ApiController
    {
        private readonly IListServices _ListService;
        public ListController(IListServices ListService)
        {
            _ListService = ListService;
        }

        [HttpGet]
        [Route("api/list/getProductTypeByParentId")]
        public async Task<IHttpActionResult> GetProductTypeByParentId(int parent_id)
        {
            try
            {
                var result = await _ListService.GetProductTypeByParentId(parent_id);

                return Ok(new ResponseObject<ProductTypeListViewModel>
                {
                    Status = "ok",
                    Data = result
                });
            }
            catch (NotFoundException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
